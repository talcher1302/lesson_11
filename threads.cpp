#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love threads\n";
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	
	t.join();
}

void printVector(vector<int> primes)
{
	int i = 0;

	for (i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << "\n";
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int num = 0;
	int i = 0, j = 0;
	int count = 0;

	if (begin == 0)
		begin++;

	for (i = begin; i <= end; i++)
	{
		count = 0;
		for (j = 2; j < i; j++)
		{
			if (i % j == 0)
				count++;
		}

		if (count <= 1)
			primes.push_back(i);
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;

	clock_t start = clock();

	std::thread t(getPrimes, begin, end, ref(primes));
	
	t.join();

	clock_t finish = clock();
	double time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << "Elapsed Time: " << time_spent << " seconds" << endl;

	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int num = 0;
	int i = 0, j = 0;
	int count = 0;

	if (begin == 0)
		begin++;

	for (i = begin; i <= end; i++)
	{
		count = 0;
		for (j = 2; j < i; j++)
		{
			if (i % j == 0)
				count++;
		}

		if (count <= 1)
			file << i << endl;
	}

	file.close();
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	std::ofstream primes(filePath);
	clock_t start, finish;
	double time_spent;


	start = clock();

	int amount = (end - begin) / N + 1;
	std::thread* arr = new std::thread[N];
	for (int i = 0; i < N; i++)
	{
		int new_end = begin + amount * (i + 1) - 1 > end ? end : begin + amount * (i + 1) - 1;
		arr[i] = std::thread(writePrimesToFile, begin + amount * i, new_end, ref(primes));
	}
	for (int i = 0; i < N; i++)
	{
		arr[i].join();
	}

	finish = clock();
	time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << "Elapsed Time: " << time_spent << " seconds" << endl;

	primes.close();

}
